﻿using AutoMapper;
using BookshellDal.DbModels;
using BookshellModels;
using BookshellServices;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;


namespace BookshellApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class ThingsController : ControllerBase
    {
        public IActionResult Get()
        {
            return Ok( new List<Thing>{new Thing{id = "123456", title = "APIAPIcall"}, new Thing{id = "123457", title = "APIAPIcall"} } );
        }

    }

    public class Thing
    {
        public string id { get; set; }
        public string title { get; set; }
    }
}