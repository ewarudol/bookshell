using AutoMapper;
using BookshellDal.DbModels;
using BookshellModels;
using BookshellServices;
using Microsoft.AspNetCore.Mvc;

namespace BookshellApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class UserDataController: Controller
    {
        private readonly IUserDataService _userDataService;
        private readonly IMapper _mapper;

        public UserDataController(IUserDataService userDataService, IMapper mapper)
        {
            _userDataService = userDataService;
            _mapper = mapper;
        }
        
        [HttpGet]
        public ApiResponse<UserData> Get()
        {
            return new ApiResponse<UserData>{Data = _userDataService.GetOrInitUserData()};
        }
        
        [HttpPost]
        public ApiResponse<UserData> Post([FromBody] UserData userData)
        {
            _userDataService.UpdateUserData(userData);
            return new ApiResponse<UserData>{Data = userData};
        }
        
        [HttpPatch]
        public ApiResponse<UserData> Patch([FromBody] UserData userData)
        {
            _userDataService.UpdateUserData(userData);
            return new ApiResponse<UserData>{Data = userData};
        }
        
    }
}