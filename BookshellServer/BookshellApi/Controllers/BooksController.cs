using System.Collections.Generic;
using AutoMapper;
using BookshellDal;
using BookshellDal.DbModels;
using BookshellModels;
using BookshellServices;
using Microsoft.AspNetCore.Mvc;

namespace BookshellApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class BooksController: Controller
    {
        private readonly IBooksService _bookService;
        private readonly IBooksCategoriesService _bookCategoriesService;
        private readonly IMapper _mapper;

        public BooksController(IBooksService bookService, IMapper mapper, IBooksCategoriesService bookCategoriesService)
        {
            _bookService = bookService;
            _bookCategoriesService = bookCategoriesService;
            _mapper = mapper;
        }
        
        [HttpGet("{id}")]
        public ApiResponse<BookDto> Get(int id)
        {
            return new ApiResponse<BookDto>{ Data =  _mapper.Map<BookDto>(_bookService.GetBook(id))};
        }
        
        [HttpGet]
        public ApiResponse<IEnumerable<BookDto>> Get()
        {
            return new ApiResponse<IEnumerable<BookDto>>{ Data =  _mapper.Map<IEnumerable<BookDto>>(_bookService.GetBooks())};
        }
        
        [HttpGet("[action]")]
        public ApiResponse<BookDto> DebugCreateSample()
        {
            var book = _bookService.CreateBook(new Book
            {
                BookName = "Wyprawa na Północ", 
                FileName = "Wyprawa.pdf", 
                Category = _bookCategoriesService.GetOrInitDefaultCategory()
            });
            
            return new ApiResponse<BookDto>{Data = _mapper.Map<BookDto>(book)};
        }

        [HttpPost("[action]")]
        public ApiResponse<string> OpenFile([FromBody] BookDto book)
        {
            var res = _bookService.OpenFile(book.PhysicalPath);
            return new ApiResponse<string>{Data = res};
        }
        
    }
    
    
}