using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BookshellDal;
using BookshellDal.DbModels;
using BookshellModels;
using BookshellServices;
using Microsoft.AspNetCore.Mvc;

namespace BookshellApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class BooksCategoriesController: Controller
    {
        private readonly IBooksCategoriesService _bookCategoriesService;
        private readonly IMapper _mapper;

        public BooksCategoriesController(IMapper mapper, IBooksCategoriesService bookCategoriesService)
        {
            _bookCategoriesService = bookCategoriesService;
            _mapper = mapper;
        }
        
        [HttpGet]
        public ApiResponse<IEnumerable<BooksCategoryDto>> Get()
        {
            
            return new ApiResponse<IEnumerable<BooksCategoryDto>>
            { 
                Data = _mapper.Map<IEnumerable<BooksCategoryDto>>(_bookCategoriesService.GetAllWithBooks())
            };
        }
        
        [HttpPost("[action]")]
        public ApiResponse<Tree> SaveTree([FromBody] Tree tree)
        {
            _bookCategoriesService.SaveTree(tree);
            return new ApiResponse<Tree>
            { 
                Data = tree
            };
        }
        
    }
}