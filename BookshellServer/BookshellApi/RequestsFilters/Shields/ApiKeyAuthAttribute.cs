﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Bookshell.RequestsFilters.Shields
{
    ///// <summary>
    ///// Classes and Methods Attribute to filter unauthorized requests.
    ///// Depends on 'ApiKeyHeaderName' and stored in appsettings 'ApplicationApiKey'
    ///// </summary>
    //[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    //public class ApiKeyAuthAttribute : Attribute, IAuthorizationFilter
    //{
    //    public void OnAuthorization(AuthorizationFilterContext context)
    //    {
    //        //get a potential value paired with 'ApiKeyHeaderName' key from Header of a Request
    //        if (!context.HttpContext.Request.Headers.TryGetValue(GlobalSettings.ApiKeyHeaderName, out var potentialApiKey))
    //        {
    //            context.Result = new UnauthorizedObjectResult(new UnifiedResponse(401, "You have no ApiKey in the header, im callin' the police"));
    //            return;
    //        }

    //        //get 'ApplicationApiKey' setting from appsettings
    //        var configuration = context.HttpContext.RequestServices.GetRequiredService<IConfiguration>();
    //        var apiKey = configuration.GetValue<string>("ApplicationApiKey");
    //        if (!apiKey.Equals(potentialApiKey))
    //        {
    //            context.Result = new UnauthorizedObjectResult(new UnifiedResponse(401, "You have bad ApiKey in the header, im callin' the police"));
    //            return;
    //        }
    //    }
    //}
}
