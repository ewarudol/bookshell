﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookshellModels
{
    public class BookDto
    {
        public long Id { get; set; }
        public string FileName { get; set; }
        public string BookName { get; set; }
        public long CategoryId { get; set; }
        public string PhysicalPath { get; set; }
        public string Author { get; set; }
        public string PicUrl { get; set; }
    }
}
