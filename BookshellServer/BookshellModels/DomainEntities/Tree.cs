using System.Collections.Generic;

namespace BookshellModels
{
    public class Tree
    {
        public List<TreeCat> children { get; set; }
    }

    public class TreeCat
    {
        public string? id { get; set; }
        public bool? isLeaf { get; set; }
        public string? name { get; set; }
        public string? pid { get; set; }
        public bool? dragDisabled { get; set; }
        public bool? addTreeNodeDisabled { get; set; }
        public bool? addLeafNodeDisabled { get; set; }
        public List<TreeBook>? children { get; set; }
    }

    public class TreeBook
    {
        public string? id { get; set; }
        public bool? isLeaf { get; set; }
        public string? name { get; set; }
        public string? pid { get; set; }
        public bool? addTreeNodeDisabled { get; set; }
        public bool? addLeafNodeDisabled { get; set; }
        public bool? delNodeDisabled { get; set; }
        public bool? editNodeDisabled { get; set; }
    }
}