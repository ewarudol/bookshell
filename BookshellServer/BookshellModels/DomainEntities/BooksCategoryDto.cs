﻿using System;
using System.Collections.Generic;

namespace BookshellModels
{
    public class BooksCategoryDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public ICollection<BookDto> Books { get; set; }
    }
}
