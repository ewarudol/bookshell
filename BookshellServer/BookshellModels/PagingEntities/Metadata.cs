﻿
using Microsoft.AspNetCore.Http;
using System;

namespace BookshellModels
{
    public class PagingInfo
    {
        public static int DefaultPageSize = 10;

        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public bool HasNextPage { get; set; }
        public bool HasPreviousPage { get; set; }
        public string NextPageUrl { get; set; }
        public string PreviousPageUrl { get; set; }

        public static PagingInfo CreateInstance<T>(PagedList<T> pagedList, HttpContext context)
        {
            var meta = new PagingInfo {
                TotalCount = pagedList.TotalCount,
                PageSize = pagedList.PageSize,
                CurrentPage = pagedList.CurrentPage,
                TotalPages = pagedList.TotalPages,
                HasNextPage = pagedList.HasNextPage,
                HasPreviousPage = pagedList.HasPreviousPage,
            };
            meta.NextPageUrl = GetPaginationUri(context, meta, 1).ToString();
            meta.PreviousPageUrl = GetPaginationUri(context, meta, -1).ToString();
            return meta;
        }

        private static string GetPaginationUri(HttpContext context, PagingInfo meta, int step)
        {
            return context.Request.Path.Value + $"?PageSize={meta.PageSize}&PageNumber={meta.CurrentPage+step}";
        }

    }
}
