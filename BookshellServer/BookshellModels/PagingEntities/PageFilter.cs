﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookshellModels
{
    public class PageFilter
    {
        public int PageSize { get; set; }

        public int PageNumber { get; set; }
    }
}
