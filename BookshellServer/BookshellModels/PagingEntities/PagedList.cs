﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BookshellModels
{
    public class PagedList<T> : List<T>
    {
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public bool HasPreviousPage => CurrentPage > 1;
        public bool HasNextPage => CurrentPage < TotalPages;
        public int? NextPageNumber => HasNextPage ? CurrentPage + 1 : (int?)null;
        public int? PreviousPageNumber => HasPreviousPage ? CurrentPage - 1 : (int?)null;

        public static PagedList<T> CreateInstance(IEnumerable<T> source, PageFilter filters)
        {
            filters.PageNumber = filters.PageNumber == 0 ? 1 : filters.PageNumber;
            filters.PageSize = filters.PageSize == 0 ? PagingInfo.DefaultPageSize : filters.PageSize;
            var count = source.Count();
            var items = source.Skip((filters.PageNumber - 1) * filters.PageSize).Take(filters.PageSize).ToList();

            var page = new PagedList<T>
            {
                TotalCount = count,
                PageSize = filters.PageSize,
                CurrentPage = filters.PageNumber,
                TotalPages = (int)Math.Ceiling(count / (double)filters.PageSize)
            };
            page.AddRange(items);

            return page;
        }
    }
}
