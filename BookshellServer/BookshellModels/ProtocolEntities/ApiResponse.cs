﻿

using Newtonsoft.Json;

namespace BookshellModels
{
    public class ApiResponse<T>
    {
        public ApiResponse()
        {
         
        }

        public T Data { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public PagingInfo PagingInfo { get; set; }
    }
}
