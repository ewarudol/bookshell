﻿using System.Collections.Generic;
using BookshellDal.DbModels;
using BookshellModels;

namespace BookshellServices
{
    public interface IBooksCategoriesService
    {
        public BooksCategory GetOrInitDefaultCategory();
        public IEnumerable<BooksCategory> GetAllWithBooks();
        public Tree SaveTree(Tree tree);
    }
}