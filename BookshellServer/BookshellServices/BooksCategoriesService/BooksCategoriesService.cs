﻿using BookshellDal.DbModels;
using BookshellDal.Repositories;
using BookshellModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BookshellServices
{
    public class BooksCategoriesService : IBooksCategoriesService
    {
        private readonly string DefaultCategoryName = "All";
        private readonly IUnitOfWork _unitOfWork;
        private ICoverScraper _scraper;

        public BooksCategoriesService(IUnitOfWork unitOfWork, ICoverScraper scraper)
        {
            _unitOfWork = unitOfWork;
            _scraper = scraper;
        }

        public IEnumerable<BooksCategory> GetAllWithBooks()
        {
            
            var userData = _unitOfWork.UserDataRepository.GetById(0).Result;
            DirectoryInfo d = new DirectoryInfo(userData.BooksDirectoryPath);
            FileInfo[] filesList = d.GetFiles("*.pdf");

            var allBooks = _unitOfWork.BooksRepository.GetAll().ToList();
            var booksToAdd = new List<Book>();
            var booksToRemove = new List<Book>();
            
            foreach (var file in filesList)
            {
                if (allBooks.FirstOrDefault(x => x.FileName == file.Name) == default)
                {
                    var name = Path.GetFileNameWithoutExtension(file.Name);
                    booksToAdd.Add(new Book
                        {
                            FileName = file.Name, 
                            BookName = name, 
                            PhysicalPath = file.FullName,
                            Author = "Nieznany",
                            PicUrl = _scraper.GetCover(name),
                            Category = GetOrInitDefaultCategory()
                        });
                }
            }

            foreach (var book in allBooks)
            {
                if (filesList.FirstOrDefault(x => x.Name == book.FileName) == default)
                {
                    booksToRemove.Add(book);
                }
            }
            
            _unitOfWork.BooksRepository.DeleteRange(booksToRemove);
            _unitOfWork.SaveChanges();
            _unitOfWork.BooksRepository.AddRange(booksToAdd);
            _unitOfWork.SaveChanges();
            
            return _unitOfWork.BooksCategoriesRepository.GetAllIncludeBooks();
        }
        
        public BooksCategory GetOrInitDefaultCategory()
        {
            var category = _unitOfWork.BooksCategoriesRepository.GetById(0).Result;
            
            if (category != default) return category;
            
            //if 0. category is null
            var defaultCategory = new BooksCategory
            {
                Id = 0,
                Name = DefaultCategoryName
            };
            _unitOfWork.BooksCategoriesRepository.Add(defaultCategory);
            _unitOfWork.SaveChanges();
            return defaultCategory;
        }
        
        public Tree SaveTree(Tree tree)
        {
            var library = _unitOfWork.BooksCategoriesRepository.GetAllIncludeBooks();
            
            //add/delete categories
            var treeCategories = tree.children;
            var catsToAdd = new List<BooksCategory>();
            var catsToRemove = new List<BooksCategory>();
            var catsToUpdate = new List<BooksCategory>();
            
            foreach (var treeCat in treeCategories)
            {
                if (library.FirstOrDefault(x => x.Id.ToString() == treeCat.id) == default)
                {
                    long id = long.Parse(treeCat.id);
                    catsToAdd.Add(new BooksCategory()
                    {
                        Id = id,
                        Name = treeCat.name
                    });
                }
            }

            foreach (var cat in library)
            {
                if (treeCategories.FirstOrDefault(x => x.id == cat.Id.ToString()) == default)
                {
                    catsToRemove.Add(cat);
                }
                else
                {
                    var sameCat = treeCategories.FirstOrDefault(x => x.id == cat.Id.ToString());
                    if (sameCat.name != cat.Name)
                    {
                        cat.Name = sameCat.name;
                        catsToUpdate.Add(cat);
                    }
                }
            }
            
            _unitOfWork.BooksCategoriesRepository.DeleteRange(catsToRemove);
            _unitOfWork.SaveChanges();
            _unitOfWork.BooksCategoriesRepository.AddRange(catsToAdd);
            _unitOfWork.SaveChanges();
            _unitOfWork.BooksCategoriesRepository.UpdateRange(catsToUpdate);
            _unitOfWork.SaveChanges();
            
            //update books
            var booksPositions = new List<Tuple<long, long>>();
            foreach (var treecat in treeCategories)
                if(treecat.children!=default)
                    foreach (var treebook in treecat.children)
                        booksPositions.Add(new Tuple<long, long>(long.Parse(treebook.id), long.Parse(treecat.id)));

            var booksToUpdate = new List<Book>();

            foreach (var cat in library)
            {
                foreach (var book in cat.Books)
                {
                    var positionRecord = booksPositions.FirstOrDefault(x => x.Item1 == book.Id);
                    if (positionRecord.Item2 != book.CategoryId)
                    {
                        book.CategoryId = positionRecord.Item2;
                        booksToUpdate.Add(book);
                    }
                }
            }
            
            _unitOfWork.BooksRepository.UpdateRange(booksToUpdate);
            _unitOfWork.SaveChanges();

            return tree;
        }
       
    }
}
