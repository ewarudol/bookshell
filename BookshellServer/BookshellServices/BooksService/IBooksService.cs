﻿using System.Collections.Generic;
using BookshellDal.DbModels;
using BookshellModels;

namespace BookshellServices
{
    public interface IBooksService
    {
        public Book GetBook(int id);
        public Book CreateBook(Book book);
        public IEnumerable<Book> GetBooks();
        public string OpenFile(string path);
    }
}