﻿using BookshellDal.DbModels;
using BookshellDal.Repositories;
using BookshellModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace BookshellServices
{
    public class BooksService : IBooksService
    {
        private readonly IUnitOfWork _unitOfWork;
        private ICoverScraper _scraper;

        public BooksService(IUnitOfWork unitOfWork, ICoverScraper scraper)
        {
            _unitOfWork = unitOfWork;
            _scraper = scraper;
        }

        public Book GetBook(int id)
        {
            return _unitOfWork.BooksRepository.GetById(id).Result; 
        }

        public IEnumerable<Book> GetBooks()
        {
            var list = new List<Book>();
            var userData = _unitOfWork.UserDataRepository.GetById(0).Result;
            
            DirectoryInfo d = new DirectoryInfo(userData.BooksDirectoryPath);
            FileInfo[] Files = d.GetFiles("*.pdf");
            foreach(FileInfo file in Files )
            {
                list.Add(new Book{ FileName = file.Name});
            }

            return list;
        }
        
        public Book CreateBook(Book book)
        {
            _unitOfWork.BooksRepository.Add(book);
            _unitOfWork.SaveChanges();
            return book;
        }
        
        public string OpenFile(string path)
        {
            //TODO: What does the Windows say?
            var res = new ShellHelper().Bash($"xdg-open '{path}'");
            return res;
        }

        public string GetCover(string phrase)
        {
            var href = _scraper.GetCover(phrase);
            return href;
        }
    }
    
}
