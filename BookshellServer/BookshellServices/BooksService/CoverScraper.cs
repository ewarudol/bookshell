using System;
using System.Linq;
using System.Net;
using HtmlAgilityPack;

namespace BookshellServices
{

    public interface ICoverScraper
    {
        public string GetCover(string bookName);
    }
    
    public class LubimyCzytacScraper : ICoverScraper{

        /// <summary>Gets the cover url.</summary>
        /// <param name="bookName">Raw name of the book.</param>
        /// <param name="msg">Output message based on the process state.</param>
        /// <returns>Url of image.</returns>
        /// <exception cref="WebException">Cover server is not resonding for given url</exception>
        /// <exception cref="NullReferenceException">Book doesn't exist in web service</exception>
        public string GetCover(string bookName) {

            try
            {
                string url = BookNamePrepareUrl(bookName);
                string href = "";
                HtmlWeb web = new HtmlWeb();
                var doc = web.Load(url);
                var nodes = doc.DocumentNode.SelectNodes("//img[@class='img-fluid lazy']");
                foreach(HtmlNode node in doc.DocumentNode.SelectNodes("//img[@class='img-fluid lazy']"))
                {
                    //TODO: ładniej zescrapować i zamienić na BIIIIG okładkę hueheu 352x500.jpg
                    var coverUrl = node.Attributes.Where(x=>x.Name=="data-src").FirstOrDefault().Value;
                    coverUrl = coverUrl.Replace("170x243", "352x500");
                    return coverUrl;
                }
                return href;
            }
            catch
            {
                return "https://cdn-lubimyczytac.pl/upload/default-book-352x500.jpg";
            }
          
        }

        /// <summary>Returns web service specific url on the base of book name.</summary>
        /// <param name="bookName">Raw name of the book.</param>
        /// <returns></returns>
        private string BookNamePrepareUrl(string bookName) {
            string url = "http://lubimyczytac.pl/szukaj/ksiazki?phrase=";
            return url + bookName.Replace(' ', '+');
        }

    }
}