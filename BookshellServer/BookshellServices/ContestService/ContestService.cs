﻿using BookshellDal.DbModels;
using BookshellDal.Repositories;
using BookshellModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BookshellServices
{
    public class ContestService : IContestService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ContestService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public PagedList<Contest> GetContests(PageFilter filters)
        {
            var contests = _unitOfWork.ContestRepository.GetAll();

            var pagedContests = PagedList<Contest>.CreateInstance(contests, filters);
            return pagedContests;
        }
    }
}
