﻿using BookshellDal.DbModels;
using BookshellModels;

namespace BookshellServices
{
    public interface IContestService
    {
        PagedList<Contest> GetContests(PageFilter filters);

    }
}