﻿using BookshellDal.DbModels;
using BookshellModels;

namespace BookshellServices
{
    public interface IUserDataService
    {
        public UserData GetOrInitUserData();
        public UserData UpdateUserData(UserData userData);
    }
}