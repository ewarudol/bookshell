﻿using BookshellDal.DbModels;
using BookshellDal.Repositories;
using BookshellModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BookshellServices
{
    public class UserDataService : IUserDataService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserDataService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public UserData GetOrInitUserData()
        {
            var userDataRows = _unitOfWork.UserDataRepository.GetAll();
            
            //if table is empty
            if (userDataRows == default || userDataRows.Count() == 0)
                return InitUserData();
            
            //if table has UserData
            return userDataRows.First();
        }

        public UserData UpdateUserData(UserData userData)
        {
            userData.Id = 0;
            _unitOfWork.UserDataRepository.Update(userData);
            _unitOfWork.SaveChanges();
            return userData;
        }

        private UserData InitUserData()
        {
            //Add UserData row 
            var preConfigIndicator = 1;
            var newUserDataRow = new UserData {IsPreConfigPhase = preConfigIndicator};
            _unitOfWork.UserDataRepository.Add(newUserDataRow);
            _unitOfWork.SaveChanges();

            return newUserDataRow;
        }
        
    }
}
