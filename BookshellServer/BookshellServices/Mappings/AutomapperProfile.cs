﻿using AutoMapper;
using BookshellDal;
using BookshellDal.DbModels;
using BookshellModels;

namespace BookshellServices
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Book, BookDto>();
            CreateMap<BookDto, Book>();
            
            CreateMap<BooksCategory, BooksCategoryDto>();
            CreateMap<BooksCategoryDto, BooksCategory>();
        }
    }
}
