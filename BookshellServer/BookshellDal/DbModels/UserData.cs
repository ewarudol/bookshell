﻿using System;
using System.Collections.Generic;

namespace BookshellDal.DbModels
{
    public partial class UserData
    {
        public long Id { get; set; }
        public long IsPreConfigPhase { get; set; }
        public string BooksDirectoryPath { get; set; }
    }
}
