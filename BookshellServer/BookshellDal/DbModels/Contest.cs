﻿using System;
using System.Collections.Generic;

namespace BookshellDal.DbModels
{
    public partial class Contest
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Hint { get; set; }
    }
}
