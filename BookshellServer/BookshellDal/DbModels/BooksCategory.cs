﻿using System;
using System.Collections.Generic;

namespace BookshellDal.DbModels
{
    public partial class BooksCategory
    {
        public long Id { get; set; }
        public string Name { get; set; }
        
        public ICollection<Book> Books { get; set; }
    }
}
