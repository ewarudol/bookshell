﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookshellDal.DbModels
{
    public partial class Book
    {
        public long Id { get; set; }
        public string FileName { get; set; }
        public string BookName { get; set; }
        public string PhysicalPath { get; set; }
        public string Author { get; set; }
        public string PicUrl { get; set; }
        
        [ForeignKey("Category")]
        public long CategoryId { get; set; }
        public BooksCategory Category { get; set; }
    }
}
