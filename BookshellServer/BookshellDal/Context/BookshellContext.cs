﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using BookshellDal.DbModels;

namespace BookshellDal.Context
{
    public partial class BookshellContext : DbContext
    {
        public BookshellContext()
        {
        }

        public BookshellContext(DbContextOptions<BookshellContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Contest> Contest { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contest>(entity =>
            {
                entity.ToTable("contest");
                entity.Property(e => e.Id).HasColumnName("ID").ValueGeneratedOnAdd();
                entity.Property(e => e.Name).HasColumnType("varchar(20)");
            });
            modelBuilder.Entity<UserData>(entity =>
            {
                entity.ToTable("UserData");
                entity.Property(e => e.Id).HasColumnName("Id").ValueGeneratedNever();
                entity.Property(e => e.IsPreConfigPhase).HasColumnName("IsPreConfigPhase");
                entity.Property(e => e.BooksDirectoryPath).HasColumnName("BooksDirectoryPath");
            });
            modelBuilder.Entity<Book>(entity =>
            {
                entity.ToTable("Books");
                entity.Property(e => e.Id).HasColumnName("Id").ValueGeneratedOnAdd();
                entity.Property(e => e.FileName).HasColumnName("FileName");
                entity.Property(e => e.BookName).HasColumnName("BookName");
                entity.Property(e => e.PhysicalPath).HasColumnName("PhysicalPath");
                entity.Property(e => e.Author).HasColumnName("Author");
                entity.Property(e => e.PicUrl).HasColumnName("PicUrl");
                entity.Property(e => e.CategoryId).HasColumnName("CategoryId");
            });
            modelBuilder.Entity<BooksCategory>(entity =>
            {
                entity.ToTable("BooksCategories");
                entity.Property(e => e.Id).HasColumnName("Id").ValueGeneratedOnAdd();
                entity.Property(e => e.Name).HasColumnName("Name");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
