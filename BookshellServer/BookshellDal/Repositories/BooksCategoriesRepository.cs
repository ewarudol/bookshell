using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BookshellDal.Context;
using BookshellDal.DbModels;
using Microsoft.EntityFrameworkCore;

namespace BookshellDal.Repositories
{
    public class BooksCategoriesRepository : BaseRepository<BooksCategory>
    {
        public BooksCategoriesRepository(BookshellContext context) : base(context)
        {
        }
        
        public IEnumerable<BooksCategory> GetAllIncludeBooks()
        {
            return _entities.Include(x=>x.Books);
        }
    }
}