﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookshellDal.Repositories
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        Task<T> GetById(long id);
        Task Add(T entity);
        void Update(T entity);
        Task Delete(long id);
        void DeleteRange(IEnumerable<T> entities);
        void AddRange(IEnumerable<T> entities);
        public void UpdateRange(IEnumerable<T> entities);
    }
}
