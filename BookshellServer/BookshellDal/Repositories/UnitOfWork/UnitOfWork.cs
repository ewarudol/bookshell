﻿using BookshellDal.Context;
using BookshellDal.DbModels;
using BookshellDal.Repositories;
using System.Threading.Tasks;

namespace BookshellDal.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BookshellContext _context;
        private readonly IRepository<Contest> _contestRepository;
        private readonly IRepository<UserData> _userDataRepository;
        private readonly IRepository<Book> _booksRepository;
        private readonly BooksCategoriesRepository _booksCategoriesRepository;

        public UnitOfWork(BookshellContext context)
        {
            _context = context;
        }

        public IRepository<Contest> ContestRepository => _contestRepository ?? new BaseRepository<Contest>(_context);
        public IRepository<UserData> UserDataRepository => _userDataRepository ?? new BaseRepository<UserData>(_context);
        
        public IRepository<Book> BooksRepository => _booksRepository ?? new BaseRepository<Book>(_context);
        
        //overwritten by custom repository
        public BooksCategoriesRepository BooksCategoriesRepository => _booksCategoriesRepository ?? new BooksCategoriesRepository(_context);

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
