﻿using BookshellDal.DbModels;
using BookshellDal.Repositories;
using System;
using System.Threading.Tasks;

namespace BookshellDal.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Contest> ContestRepository { get; }
        IRepository<UserData> UserDataRepository { get; }
        public IRepository<Book> BooksRepository { get; }
        public BooksCategoriesRepository BooksCategoriesRepository { get; }

        void SaveChanges();

        Task SaveChangesAsync();
    }
}
