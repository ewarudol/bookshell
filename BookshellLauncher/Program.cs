﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

namespace BookshellLauncher
{
    using System;
    using System.Diagnostics;
    public class ShellHelper
    {
        public string Bash(string cmd)
        {
            var escapedArgs = cmd.Replace("\"", "\\\"");
                
            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };
            process.Start();
            string result = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
            return result;
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            string path = Directory.GetCurrentDirectory();
            Console.WriteLine("App directory:");
            Console.WriteLine(path);
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                new ShellHelper().Bash($"cd {path}/EngineData ; ./BookshellApi & cd {path}/Render ; ./vue-electron-app &");
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                Console.WriteLine("winda");
            }
            else
                Console.WriteLine("Ooops, Mac is not supported, sory boiz");
        }
    }
}
