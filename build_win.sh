echo "Prepare windows public"
cd "$(dirname "$0")"
dotnet publish -o "Public/Public_win/EngineData"
echo "Host App published"
dotnet publish BookshellLauncher -o Public/Public_win
echo "Launcher published"
cd BookshellClient
./node_modules/.bin/vue-cli-service electron:build --windows
cd dist_electron
cp -r win-unpacked ../../Public/Public_win/Render
echo "Electron published"