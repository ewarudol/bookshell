import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueTreeList from 'vue-tree-list'

Vue.use(VueTreeList)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

