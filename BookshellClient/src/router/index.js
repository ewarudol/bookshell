import Vue from 'vue'
import VueRouter from 'vue-router'
import Bookshell from '../views/Bookshell.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/Bookshell',
    name: 'Bookshell',
    component: Bookshell
  }
]

const router = new VueRouter({
  routes: routes,
  mode: 'history'
})

export default router
