echo "Prepare linux public"
cd "$(dirname "$0")"
dotnet publish -r linux-x64 -o "Public/Public_linux/EngineData"
echo "Host App published"
dotnet publish BookshellLauncher -r linux-x64 -o Public/Public_linux
echo "Launcher published"
cd BookshellClient
npm run electron:build
cd dist_electron
cp -r linux-unpacked ../../Public/Public_linux/Render
echo "Electron published"